(in-package :bearlibterminal-examples)

(defun help-msg (player-char)
  (terminal-print 0 0 (format nil "Use arrow keys to move your character represented by ~C!" player-char))
  (terminal-print 0 1 "Exit with the escape button."))

(defun movement (&key (player-codepoint #x2638) (window-x 80) (window-y 25))
  (let ((player-char (code-char player-codepoint)))
    (terminal-open)
    (terminal-set (format nil "window: size=~Dx~D, title='BearLibTerminal movement example';" window-x window-y))
    ;; Set separately so the other settings take even if the font isn't found
    ;; Make sure you are in the right directory/change the font path if it doesn't load.
    (terminal-set "font: ./fonts/DejaVuSansMono.ttf, size=12")
    (help-msg player-char)
    (terminal-put 5 5 player-codepoint)
    (terminal-refresh)
    (loop with player-x = 5
          with player-y = 5
          as key = (terminal-read) do
            (cond
              ((= key bearlibterminal-ffi:+tk-left+) (decf player-x))
              ((= key bearlibterminal-ffi:+tk-right+) (incf player-x))
              ((= key bearlibterminal-ffi:+tk-down+) (incf player-y))
              ((= key bearlibterminal-ffi:+tk-up+) (decf player-y)))
            (terminal-clear)
            (help-msg player-char)
            (terminal-put player-x player-y player-codepoint)
            (terminal-refresh)
          until (or (= key bearlibterminal-ffi:+tk-escape+)
                    (= key bearlibterminal-ffi:+tk-close+)))
    (terminal-close)))
