;;;; cl-bearlibterminal.asd

(asdf:defsystem #:cl-bearlibterminal
  :description "A simple interface to the easy to use terminal library bearlibterminal, designed to help write text based games."
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "BSD 3-clause"
  :serial t
  :depends-on ("cl-autowrap")
  :components ((:file "package")
               (:file "cl-bearlibterminal")))
