(in-package :bearlibterminal-examples)

(defun hello-world ()
  "Simple hello world terminal example"
  (terminal-open)
  (terminal-print 1 1 "Hello, world!")
  (terminal-refresh)
  (loop while (not (equal (terminal-read) bearlibterminal-ffi:+tk-close+)))
  (terminal-close))
