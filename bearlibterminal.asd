;;;; bearlibterminal.asd

(asdf:defsystem #:bearlibterminal
  :description "A simple interface to the easy to use terminal library bearlibterminal, designed to help write text based games."
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "BSD 3-clause"
  :serial t
  :pathname "src"
  :depends-on ("cl-autowrap"
               "cl-plus-c")
  :components ((:module :autowrap-spec
                :pathname "spec"
                :components
                ((:static-file "BearLibTerminal.h")))
               (:file "package")
               (:file "library")
               (:file "autowrap")
               (:file "bearlibterminal-internal")))

(asdf:defsystem #:bearlibterminal/examples
  :description "simple examples to demonstrate common usage of bearlibterminal."
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "BSD 3-clause"
  :depends-on (:bearlibterminal)
  :pathname "examples"
  :serial t
  :components ((:file "hello-world")
               (:file "hello-name")))
