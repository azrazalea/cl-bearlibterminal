(cl:in-package :bearlibterminal-ffi)

(autowrap:c-include
 '(bearlibterminal autowrap-spec "BearLibTerminal.h")
  :accessor-package :bearlibterminal-ffi.accessors
  :function-package :bearlibterminal-ffi.functions
  :spec-path '(bearlibterminal autowrap-spec)
  :exclude-sources ("/usr/local/lib/clang/([^/]*)/include/(?!stddef.h)"
                    "/usr/include/"
                    "/usr/include/arm-linux-gnueabihf")
  :include-sources ("stdint.h"
                    "bits/types.h"
                    "sys/types.h"
                    "BearLibTerminal.h")
  :exclude-definitions ("terminal_read_str"
                        "terminal_get"
                        "terminal_print"
                        "terminal_set"
                        "terminal_measure"
                        "color_from_name"
                        "color_from_argb")
  :no-accessors cl:t
  :release-p cl:t)
