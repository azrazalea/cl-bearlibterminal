;;;; bearlibterminal-internal.lisp

(in-package #:bearlibterminal-internal)

(defun terminal-print (x y string)
  (terminal-print8 x y string))

(defun terminal-set (string)
  (terminal-set8 string))

(defun terminal-measure (string)
  (terminal-measure8 string))

(defun terminal-read-str (x y max)
  (let ((input-buffer (cffi:foreign-alloc :char :initial-element 0 :count max)))
    (terminal-read-str8 x y input-buffer max)
    (let ((input-string (cffi:foreign-string-to-lisp input-buffer)))
      (cffi:foreign-free input-buffer)
      input-string)))

(defun terminal-get (key default)
  (terminal-get8 key default))

(defun color-from-name (name)
  (color-from-name8 name))
