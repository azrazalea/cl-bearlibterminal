;;;; package.lisp

(defpackage #:bearlibterminal
  (:use) ; nothing
  (:export
   ;; In order of http://foo.wyrd.name/en:bearlibterminal:reference
   #:terminal-open
   #:terminal-close
   #:terminal-set
   #:terminal-get
   #:terminal-color
   #:terminal-bkcolor
   #:terminal-composition
   #:terminal-layer
   #:terminal-clear
   #:terminal-clear-area
   #:terminal-crop
   #:terminal-refresh
   #:terminal-put
   #:terminal-pick
   #:terminal-pick-color
   #:terminal-pick-bkcolor
   #:terminal-put-ext
   #:terminal-print
   #:terminal-measure
   #:terminal-state
   #:terminal-check
   #:terminal-has-input
   #:terminal-read
   #:terminal-peek
   #:terminal-read-str
   #:terminal-delay
   #:color-from-name))

(defpackage #:bearlibterminal-ffi)
(defpackage #:bearlibterminal-ffi.accessors)
(defpackage #:bearlibterminal-ffi.functions
  (:use #:bearlibterminal))

(defpackage #:bearlibterminal-internal
  (:use #:cl
        #:autowrap.minimal
        #:plus-c
        #:bearlibterminal
        #:bearlibterminal-ffi.functions))

(defpackage #:bearlibterminal-examples
  (:use :cl :bearlibterminal)
  (:export
   #:hello-world
   #:hello-name
   #:movement))
