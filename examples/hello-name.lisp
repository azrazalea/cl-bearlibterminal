(in-package :bearlibterminal-examples)

(defun hello-name (&key (delay 5000))
  "Ask for your name, display, then close after delay seconds"
  (terminal-open)
  (terminal-print 1 1 "What is your name?")
  (terminal-refresh)
  (let ((name (terminal-read-str 1 2 256)))
    (terminal-print 1 3 (format nil "Hello ~A! Nice to meet you!" name)))
  (terminal-refresh)
  (terminal-delay delay)
  (terminal-close))
